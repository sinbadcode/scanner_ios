﻿using System;
using SQLite;
using SQLite.Net.Attributes;

namespace XaScanner_IOS
{
	public class Document
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string path { get; set; }
		public string thumbnailPath { get; set; }
		public Document ()
		{
		}

	}
}

