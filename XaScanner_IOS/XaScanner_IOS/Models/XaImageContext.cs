﻿using System;
using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using System.Drawing;

namespace XaScanner_IOS
{
	public class XaImageContext
	{

		static UIImage _originalImage 	= null;
		static UIImage _croppedImage 	= null;
		static UIImage _filteringImage 	= null;

		/// <summary>
		/// Gets or sets the original image.
		/// </summary>
		/// <value>The original image.</value>
		public static UIImage OriginalImage 
		{
			get
			{
				return _originalImage;
			}

			set
			{
				if (_originalImage != null)
					_originalImage.Dispose ();
				_originalImage = value;
			}
		}

		/// <summary>
		/// Gets or sets the cropped image.
		/// </summary>
		/// <value>The cropped image.</value>
		public static UIImage CroppedImage 
		{
			get
			{
				return _croppedImage;
			}

			set
			{
				if (_croppedImage != null)
					_croppedImage.Dispose ();
				_croppedImage = value;
			}
		}

		/// <summary>
		/// Gets or sets the processing image.
		/// </summary>
		/// <value>The processing image.</value>
		public static UIImage FilteringImage 
		{
			get
			{
				return _filteringImage;
			}

			set
			{
				if (_filteringImage != null)
					_filteringImage.Dispose ();
				_filteringImage = value;
			}
		}

		/// <summary>
		/// Clone the specified image.
		/// </summary>
		/// <param name="image">Image.</param>
		public static UIImage Clone(UIImage image)
		{
			CGImage cgi = image.CGImage.Clone();
			UIImage res = new UIImage (cgi,image.CurrentScale,image.Orientation);
			return res;
		}

		/// <summary>
		/// Clone the specified image and orientation.
		/// </summary>
		/// <param name="image">Image.</param>
		/// <param name="orientation">Orientation.</param>
		public static UIImage Clone(UIImage image, UIImageOrientation orientation)
		{
			CGImage cgi = image.CGImage.Clone();
			UIImage res = new UIImage (cgi,image.CurrentScale,orientation);
			return res;
		}

		/// <summary>
		/// /*Rect2s the quad.*/
		/// </summary>
		/// <returns>The quad.</returns>
		/// <param name="rect">Rect.</param>
		/// <param name="x1a">X1a.</param>
		/// <param name="y1a">Y1a.</param>
		/// <param name="x2a">X2a.</param>
		/// <param name="y2a">Y2a.</param>
		/// <param name="x3a">X3a.</param>
		/// <param name="y3a">Y3a.</param>
		/// <param name="x4a">X4a.</param>
		/// <param name="y4a">Y4a.</param>
		public static CATransform3D Rect2Quad(CGRect rect, nfloat x1a, nfloat y1a,nfloat x2a, nfloat y2a,nfloat x3a, nfloat y3a,nfloat x4a, nfloat y4a)
		{
			nfloat X = rect.Location.X;
			nfloat Y = rect.Location.Y;
			nfloat W = rect.Size.Width;
			nfloat H = rect.Size.Height;

			nfloat y21 = y2a - y1a;
			nfloat y32 = y3a - y2a;
			nfloat y43 = y4a - y3a;
			nfloat y14 = y1a - y4a;
			nfloat y31 = y3a - y1a;
			nfloat y42 = y4a - y2a;

			nfloat a = -H * (x2a * x3a * y14 + x2a * x4a * y31 - x1a * x4a * y32 + x1a * x3a * y42);
			nfloat b = W * (x2a * x3a * y14 + x3a * x4a * y21 + x1a * x4a * y32 + x1a * x2a * y43);
			nfloat c = H * X * (x2a * x3a * y14 + x2a * x4a * y31 - x1a * x4a * y32 + x1a * x3a * y42) - H * W * x1a * (x4a * y32 - x3a * y42 + x2a * y43) - W * Y * (x2a * x3a * y14 + x3a * x4a * y21 + x1a * x4a * y32 + x1a * x2a * y43);

			nfloat d = H * (-x4a * y21 * y3a + x2a * y1a * y43 - x1a * y2a * y43 - x3a * y1a * y4a + x3a * y2a * y4a);
			nfloat e = W * (x4a * y2a * y31 - x3a * y1a * y42 - x2a * y31 * y4a + x1a * y3a * y42);
			nfloat f = -(W * (x4a * (Y * y2a * y31 + H * y1a * y32) - x3a * (H + Y) * y1a * y42 + H * x2a * y1a * y43 + x2a * Y * (y1a - y3a) * y4a + x1a * Y * y3a * (-y2a + y4a)) - H * X * (x4a * y21 * y3a - x2a * y1a * y43 + x3a * (y1a - y2a) * y4a + x1a * y2a * (-y3a + y4a)));

			nfloat g = H * (x3a * y21 - x4a * y21 + (-x1a + x2a) * y43);
			nfloat h = W * (-x2a * y31 + x4a * y31 + (x1a - x3a) * y42);
			nfloat i = W * Y * (x2a * y31 - x4a * y31 - x1a * y42 + x3a * y42) + H * (X * (-(x3a * y21) + x4a * y21 + x1a * y43 - x2a * y43) + W * (-(x3a * y2a) + x4a * y2a + x2a * y3a - x4a * y3a - x2a * y4a + x3a * y4a));

			nfloat kEpsilon = 0.0001f;

			if(Math.Abs(i) < kEpsilon)
			{
				i = kEpsilon* (i > 0 ? 1.0f : -1.0f);
			}

			CATransform3D transform = new CATransform3D ();
			transform.m11 = a / i;
			transform.m12 = d / i;
			transform.m13 = 0;
			transform.m14 = g / i;
			transform.m21 = b / i;
			transform.m22 = e / i;
			transform.m23 = 0;
			transform.m24 = h / i;
			transform.m31 = 0;
			transform.m32 = 0;
			transform.m33 = 1;
			transform.m34 = 0;
			transform.m41 = c / i;
			transform.m42 = f / i;
			transform.m43 = 0;
			transform.m44 = 1;

			return transform;
		}

		/// <summary>
		/// Rect2s the quad.
		/// </summary>
		/// <returns>The quad.</returns>
		/// <param name="rect">Rect.</param>
		/// <param name="topLeft">Top left.</param>
		/// <param name="topRight">Top right.</param>
		/// <param name="bottomLeft">Bottom left.</param>
		/// <param name="bottomRight">Bottom right.</param>
		public static CATransform3D Rect2Quad(CGRect rect,CGPoint topLeft, CGPoint topRight, CGPoint bottomLeft, CGPoint bottomRight)
		{
			return XaImageContext.Rect2Quad(rect, topLeft.X, topLeft.Y, topRight.X, topRight.Y, bottomLeft.X, bottomLeft.Y, bottomRight.X, bottomRight.Y);
		}
	}
}