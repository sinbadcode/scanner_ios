﻿using System;
using SQLite;
using System.Linq;
using System.Collections.Generic;
using SQLite.Net;

namespace XaScanner_IOS
{
	public class XaRepository
	{
		private SQLiteConnection _connection;

		public XaRepository(SQLiteConnection connection)
		{
			_connection = connection;
			_connection.CreateTable<Document>();
		}

		public int Create(Document document)
		{
			int x = _connection.Insert(document);
			_connection.Commit();
			return x;
		}

		public void Delete(Document document)
		{
			_connection.Delete(document);
			_connection.Commit();
		}

		public Document Get(int documentId)
		{
			var query = from c in _connection.Table<Document>()
					where c.ID == documentId
				select c;

			return query.FirstOrDefault ();
		}

		public List<Document> GetAll()
		{
			var query = from c in _connection.Table<Document>()
				select c;
			return query.ToList();
		}

		public void Update(Document document)
		{
			_connection.Update(document);
			_connection.Commit();
		}
	}
}

