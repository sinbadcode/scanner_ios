using System;
using System.Drawing;
using Foundation;
using UIKit;

using MonoTouch.GPUImage;
using MonoTouch.GPUImage.Filters;
using CoreGraphics;
using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using System.Collections.Generic;
using mTouchPDFReader.Library.Views.Core;

namespace XaScanner_IOS
{
	public partial class XaFiltersPhotoVC : UIViewController
	{

		private SQLiteConnection _connection;
		private Document doc = new Document ();
		LoadingOverlay loadingOverlay;
		/// <summary>
		/// The source picture.
		/// </summary>
		GPUImagePicture SourcePicture { get; set;}

		/// <summary>
		/// Initializes a new instance of the <see cref="XaScanner.XaFiltersPhotoVC"/> class.
		/// </summary>
		public XaFiltersPhotoVC () : base ("XaFiltersPhotoVC", null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="XaScanner.XaFiltersPhotoVC"/> class.
		/// </summary>
		/// <param name="handle">Handle.</param>
		public XaFiltersPhotoVC (IntPtr handle) : base (handle)
		{
		}

		/// <summary>
		/// Gets the image view.
		/// </summary>
		/// <value>The image view.</value>
		UIImageView imageView
		{
			get
			{ 
				return this.View.ViewWithTag (1000) as UIImageView;
			}
		}

		/// <Docs>Called when the system is running low on memory.</Docs>
		/// <summary>
		/// Dids the receive memory warning.
		/// </summary>
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		/// <summary>
		/// Updates the image.
		/// </summary>
		void UpdateImage()
		{
			this.imageView.Image = XaImageContext.FilteringImage;
		}

		/// <summary>
		/// Views the did load.
		/// </summary>
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Setup the database connection
			var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string dbFilePath = System.IO.Path.Combine (documentsDirectory, "xadb.db"); // hardcoded filename, overwritten each time

			_connection = new SQLiteConnection (new SQLitePlatformIOS(), dbFilePath);

			this.imageView.ContentMode = UIViewContentMode.ScaleAspectFit;

			btnDone.TouchUpInside += delegate 
			{
				saveDocument();
			};

			btnDoneAndView.TouchUpInside += delegate {
				viewDocument();
			};

			XaImageContext.FilteringImage = XaImageContext.Clone(XaImageContext.CroppedImage);
			this.SourcePicture = new GPUImagePicture(XaImageContext.CroppedImage);
			this.UpdateImage ();
		}

		void viewDocument(){
			// Determine the correct size to start the overlay (depending on device orientation)
			var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
			if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
				bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
			}
			// show the loading overlay on the UI thread using the correct orientation sizing
			this.loadingOverlay = new LoadingOverlay (bounds);
			this.View.Add ( this.loadingOverlay );
			//PDFHelper.WriteSingleImage (this.imageView.Image);
			PDFHelper.PreViewPDF (this.imageView.Image, 500f, 700f);
			loadingOverlay.Hide ();
			var docId = 1;
			var docName = "Demo PDF";
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string docFilePath = System.IO.Path.Combine (documentsDirectory, "tmp.pdf");

			var docViewController = new DocumentVC (docId, docName, docFilePath);
			this.NavigationController.PushViewController(docViewController, true);
		}

		void saveDocument()
		{
			// Determine the correct size to start the overlay (depending on device orientation)
			var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
			if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
				bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
			}
			// show the loading overlay on the UI thread using the correct orientation sizing
			this.loadingOverlay = new LoadingOverlay (bounds);
			this.View.Add ( this.loadingOverlay );

			var repository = new XaRepository (_connection);
			doc.path = "";
			var id = repository.Create (doc);
			if (id == 1) {
				var documentsDirectory = Environment.GetFolderPath
					(Environment.SpecialFolder.Personal);
				//get time
				long milliseconds = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
				string jpgFilename = milliseconds + "_document.jpg";
				string jpgFilePath = System.IO.Path.Combine (documentsDirectory, jpgFilename); 
				NSData imgData = this.imageView.Image.AsJPEG();
				NSError err = null;
				if (imgData.Save(jpgFilePath, false, out err)) {
					doc.path = jpgFilename;
				} else {
					//Console.WriteLine("NOT saved as " + jpgFilename + " because" + err.LocalizedDescription);
				}
				//save thumbnail
				string jpgThumbnailName = "thumbnail_" + jpgFilename;
				string jpgThumbnailPath = System.IO.Path.Combine (documentsDirectory, jpgThumbnailName); 
				float hightThumbnail = 200;
				float wightThumbnail = (float)((200 / this.imageView.Image.Size.Height) * this.imageView.Image.Size.Width);
				NSData thumbnailData = ImageUtils.ResizeImage (this.imageView.Image,wightThumbnail, hightThumbnail).AsJPEG();
				//ImageUtils.ResizeImage (this.imageView.Image,wightThumbnail, hightThumbnail);
				if (thumbnailData.Save(jpgThumbnailPath, false, out err)) {
					doc.thumbnailPath = jpgThumbnailName;
					repository.Update (doc);
				} else {
					//Console.WriteLine("NOT saved as " + jpgFilename + " because" + err.LocalizedDescription);
				}

				imgData = null;
				thumbnailData = null;
			}
			loadingOverlay.Hide ();
			this.NavigationController.PopToRootViewController (true);
		}



		/// <summary>
		/// Raises the auto filter event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void OnAutoFilter (UIButton sender)
		{
			XaImageContext.FilteringImage = XaImageContext.Clone(XaImageContext.CroppedImage);
			InvokeOnMainThread (UpdateImage);
		}

		/// <summary>
		/// Raises the BW filter event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void OnBWFilter (UIButton sender)
		{
			GPUImageAdaptiveThresholdFilter bwFilter = new GPUImageAdaptiveThresholdFilter();
			SourcePicture.RemoveAllTargets();
			SourcePicture.AddTarget(bwFilter);
			bwFilter.BlurRadiusInPixels = 2.0f;
			bwFilter.UseNextFrameForImageCapture();
			SourcePicture.ProcessImage();
			XaImageContext.FilteringImage = bwFilter.ImageFromCurrentFramebufferWithOrientation(XaImageContext.CroppedImage.Orientation);
			InvokeOnMainThread (UpdateImage);
		}

		/// <summary>
		/// Raises the grayscale filter event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void OnGrayscaleFilter (UIButton sender)
		{
			GPUImageGrayscaleFilter grayscaleFilter = new GPUImageGrayscaleFilter();
			SourcePicture.RemoveAllTargets();
			SourcePicture.AddTarget(grayscaleFilter);
			grayscaleFilter.UseNextFrameForImageCapture();
			SourcePicture.ProcessImage();
			XaImageContext.FilteringImage = grayscaleFilter.ImageFromCurrentFramebufferWithOrientation(XaImageContext.CroppedImage.Orientation);
			InvokeOnMainThread (UpdateImage);
		}

		/// <summary>
		/// Raises the lighten filter event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void OnLightenFilter (UIButton sender)
		{		
			GPUImageExposureFilter lightenFilter = new GPUImageExposureFilter();
			SourcePicture.RemoveAllTargets();
			SourcePicture.AddTarget(lightenFilter);
			lightenFilter.Exposure = 0.4f;
			lightenFilter.UseNextFrameForImageCapture();
			SourcePicture.ProcessImage();
			XaImageContext.FilteringImage = lightenFilter.ImageFromCurrentFramebufferWithOrientation(XaImageContext.CroppedImage.Orientation);
			InvokeOnMainThread (UpdateImage);
		}
	}
}
