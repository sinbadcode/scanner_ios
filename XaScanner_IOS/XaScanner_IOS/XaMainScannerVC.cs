using System;
using System.Collections.Generic;
using System.Drawing;
using Foundation;
using UIKit;
using SQLite;
using SQLite.Net.Platform.XamarinIOS;
using SQLite.Net;
using CoreGraphics;
using mTouchPDFReader.Library.Views.Core;

namespace XaScanner_IOS
{
	public partial class XaMainScannerVC : UIViewController
	{
		static NSString docCellId = new NSString ("MyCell");
		private SQLiteConnection _connection;
		public List<Document> _listDocuments { get; set; }
		public List<Document> _listSelected { get; set;}
		LoadingOverlay loadingOverlay;

		public XaMainScannerVC (IntPtr handle) : base (handle)
		{
			
		}
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			_listSelected = new List<Document> ();
			// Setup the database connection
			var documentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string dbFilePath = System.IO.Path.Combine (documentsDirectory, "xadb.db"); // hardcoded filename, overwritten each time

			_connection = new SQLiteConnection (new SQLitePlatformIOS(), dbFilePath);
			// Perform any additional setup after loading the view, typically from a nib.
			BtnScanner.TouchUpInside += (object sender, EventArgs e) =>{
				UIImagePickerController picker = new UIImagePickerController ();
				picker.SourceType = UIImagePickerControllerSourceType.Camera;
				picker.Delegate = new XaImagePickerDelegate(this);
				picker.CameraDevice = UIImagePickerControllerCameraDevice.Rear;
				this.PresentViewController(picker,true,null);
			};
			btnDelete.TouchUpInside += (object sender, EventArgs e) =>{
				this.deleteDocuments();
			};

			btnExport.TouchUpInside += (object sender, EventArgs e) => {
				this.exportPDF();
			};
			this.getDataForCV ();

			UICollectionViewFlowLayout flowLayout = new UICollectionViewFlowLayout ();
			flowLayout.ScrollDirection = UICollectionViewScrollDirection.Vertical;
			var cellSize = (this.__collectionView.Bounds.Width / 2) - 10;
			flowLayout.ItemSize = new CGSize (cellSize, cellSize);

			this.__collectionView.SetCollectionViewLayout (flowLayout, true);

			this.__collectionView.RegisterClassForCell (typeof(MyCell), docCellId);

			this.__collectionView.AllowsMultipleSelection = true;
			this.__collectionView.Source = new CollectionViewSource (this);

		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		override public void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear (animated);
			getDataForCV ();
			__collectionView.ReloadData ();
			_listSelected = new List<Document>();
			System.Console.WriteLine ("Call ViewDidAppear");
		}

		override public void ViewWillAppear(bool animated)
		{
//			System.Console.WriteLine ("Call ViewWillAppear");
//			base.ViewWillAppear (animated);
//			getDataForCV ();
//			__collectionView.ReloadData ();
		}

		void getDataForCV()
		{
			this._listDocuments = new List<Document> ();
			var repository = new XaRepository (_connection);
			this._listDocuments = repository.GetAll ();
		}

		void exportPDF(){
			if (_listSelected.Count == 0) {
				Commom.showAlert ("No documents selected!", this);	
				return;
			}
			// Determine the correct size to start the overlay (depending on device orientation)
			var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
			if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
				bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
			}
			// show the loading overlay on the UI thread using the correct orientation sizing
			this.loadingOverlay = new LoadingOverlay (bounds);
			this.View.Add ( this.loadingOverlay );
			var doc = _listDocuments[0];
			PDFHelper.CreatePDF (_listSelected, 500f, 700f);
			var docId = 1;
			var docName = "Demo PDF";
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string docFilePath = System.IO.Path.Combine (documentsDirectory, "tmp.pdf");
			var docViewController = new DocumentVC (docId, docName, docFilePath);
			loadingOverlay.Hide ();
			this.NavigationController.PushViewController(docViewController, true);

		}

		void deleteDocuments(){
			if (_listSelected.Count == 0) {
				Commom.showAlert ("No documents selected!", this);	
				return;
			}
			// Determine the correct size to start the overlay (depending on device orientation)
			var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
			if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
				bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
			}
			// show the loading overlay on the UI thread using the correct orientation sizing
			this.loadingOverlay = new LoadingOverlay (bounds);
			this.View.Add ( this.loadingOverlay );

			var repository = new XaRepository (_connection);
			foreach (Document value in _listSelected) {
				repository.Delete (value);
				ImageUtils.deleteImageInDirectoryByName (value.path);
			}
			repository = null;
			this.getDataForCV ();
			__collectionView.ReloadData ();
			_listSelected = new List<Document>();
			loadingOverlay.Hide ();
		}

		//******************* for collection view *********
		/// <summary>
		/// Raises the get items count event.
		/// </summary>
		/// <param name="collectionView">Collection view.</param>
		/// <param name="section">Section.</param>
		public nint OnGetItemsCount(UICollectionView collectionView, nint section)
		{
			return _listDocuments.Count;			
		}

		public  UICollectionViewCell OnGetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			var docCell = (MyCell)collectionView.DequeueReusableCell (docCellId, indexPath);

			if (String.IsNullOrEmpty (_listDocuments [indexPath.Row].path)) {
				docCell.ImageBackground = UIImage.FromFile ("error.png");
			} else {
				docCell.ImageBackground = ImageUtils.getImageByName(_listDocuments [indexPath.Row].thumbnailPath);
			}

			if (_listSelected.Contains (_listDocuments [indexPath.Row])) {
				docCell.ShowSelected = false;
			} else {
				docCell.ShowSelected = true;
			}

			//docCell.ImageView.BringSubviewToFront (docCell.ContentView);

			return docCell;
		}

		public void OnItemSelected (UICollectionView collectionView, NSIndexPath indexPath)
		{
			
		}
		public bool ShouldSelectItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			//System.Console.WriteLine ("Selected IndexPath: ({0}, {1})",indexPath.Section ,indexPath.Row);
			if (!_listSelected.Contains (_listDocuments[indexPath.Row])) {
				_listSelected.Add (_listDocuments[indexPath.Row]);
			}
			var cell = (MyCell)__collectionView.CellForItem (indexPath);
			cell.ShowSelected = false;

//			if (_listSelected.Contains (_listDocuments [indexPath.Row])) {
//				cell.ShowSelected = false;
//				return true;
//			} else {
//				cell.ShowSelected = true;
//				return false;
//			}
			return true;
		}

		public bool ShouldDeselectItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			//System.Console.WriteLine ("Deselected IndexPath: ({0}, {1})",indexPath.Section ,indexPath.Row);
			_listSelected.Remove (_listDocuments[indexPath.Row]);
			var cell = (MyCell)__collectionView.CellForItem (indexPath);
			cell.ShowSelected = true;
			return true;
		}
		//********** end ***********
	}

	public class CollectionViewSource : UICollectionViewSource
	{
		XaMainScannerVC __vc;

		public CollectionViewSource(XaMainScannerVC vc)
		{
			__vc = vc;
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return __vc.OnGetCell (collectionView, indexPath);
		}

		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return __vc.OnGetItemsCount (collectionView, section);
		}

		public override bool ShouldSelectItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return __vc.ShouldSelectItem (collectionView, indexPath);
		}

		public override bool ShouldDeselectItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return __vc.ShouldDeselectItem (collectionView, indexPath);
		}
	}



}
