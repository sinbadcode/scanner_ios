// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace XaScanner_IOS
{
	[Register ("XaMainScannerVC")]
	partial class XaMainScannerVC
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UICollectionView __collectionView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnDelete { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnExport { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton BtnScanner { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIView header { get; set; }

		[Action ("OnTakePhoto:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnTakePhoto (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (__collectionView != null) {
				__collectionView.Dispose ();
				__collectionView = null;
			}
			if (btnDelete != null) {
				btnDelete.Dispose ();
				btnDelete = null;
			}
			if (btnExport != null) {
				btnExport.Dispose ();
				btnExport = null;
			}
			if (BtnScanner != null) {
				BtnScanner.Dispose ();
				BtnScanner = null;
			}
			if (header != null) {
				header.Dispose ();
				header = null;
			}
		}
	}
}
