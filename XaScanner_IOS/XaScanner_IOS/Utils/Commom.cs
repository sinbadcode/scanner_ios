﻿using System;
using UIKit;
using CoreGraphics;
using SCImage;

namespace XaScanner_IOS
{
	public class Commom
	{
		public Commom ()
		{
		}
		public static void showAlert(string msg, UIViewController _cv)
		{
			var okAlertController = UIAlertController.Create ("Oops!", msg, UIAlertControllerStyle.Alert);
			okAlertController.AddAction (UIAlertAction.Create ("OK", UIAlertActionStyle.Default, null));
			_cv.PresentViewController (okAlertController, true, null);
		}

		public static double distance(CGPoint p1, CGPoint p2){
			double result = 0;
			result = Math.Sqrt (Math.Pow ((p1.X-p2.X), 2) + Math.Pow ((p1.Y-p2.Y), 2));
			return result;
		}
	}
}

