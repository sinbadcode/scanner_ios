﻿using System;
using Foundation;
using UIKit;
using System.Drawing;
using CoreGraphics;
using System.Collections.Generic;

namespace XaScanner_IOS
{
	public class PDFHelper
	{
		static float wPage = 595f;
		static float hPage = 842f;
		static float wImage;
		static float hImage;

		public PDFHelper ()
		{
		}

		public static void savePDF(){
			//CFAttributedStringRef currentText = new CFAttributedStringCreate(NULL, (CFStringRef)textView.text, NULL);

		}

		public static void CreatePDF (List<Document> docs, float w, float h)
		{
			int pageNum = 1;
			NSMutableData data = new NSMutableData ();
			UIGraphics.BeginPDFContext (data, new RectangleF(0,0, wPage, hPage), null);
			foreach (Document doc in docs) {
				UIGraphics.BeginPDFPage (); 
				CGContext gctx = UIGraphics.GetCurrentContext (); 
				gctx.ScaleCTM (1, -1);
				gctx.SetStrokeColor(UIColor.White.CGColor);
				//gctx.SetFillColor (UIColor.Orange.CGColor);
				UIImage image = ImageUtils.getImageByName (doc.path);
				autoGenSize (image);
				gctx.DrawImage (new RectangleF((wPage - wImage) / 2, -(hImage + (hPage - hImage) / 2), wImage, hImage),image.CGImage);
				gctx.SelectFont ("Helvetica", 25f, CGTextEncoding.MacRoman);
				gctx.ShowTextAtPoint((wPage/2 - 5), -(hPage - 10) ,"P-" + pageNum);
				pageNum += 1; 
				image = null;
			}

			UIGraphics.EndPDFContent ();

			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string pdfFilePath = System.IO.Path.Combine (documentsDirectory, "tmp.pdf"); 
			NSError err = null;
			if (data.Save(pdfFilePath, false, out err)) {
				Console.WriteLine("Saved as " + pdfFilePath);
			} else {
				Console.WriteLine("NOT saved as " + pdfFilePath + " because" + err.LocalizedDescription);
			}
			data = null;
		}


		public static NSData PreViewPDF (UIImage image, float w, float h)
		{
			NSMutableData data = new NSMutableData ();
			UIGraphics.BeginPDFContext (data, new RectangleF(0,0, wPage, hPage), null);
			UIGraphics.BeginPDFPage (); 
			CGContext gctx = UIGraphics.GetCurrentContext (); 
			gctx.ScaleCTM (1, -1);
			gctx.SetStrokeColor(UIColor.White.CGColor);
			//gctx.SetFillColor (UIColor.Orange.CGColor);
			autoGenSize (image);
			gctx.DrawImage (new RectangleF((wPage - wImage) / 2, -(hImage + (hPage - hImage) / 2), wImage, hImage),image.CGImage);
			gctx.SelectFont ("Helvetica", 25f, CGTextEncoding.MacRoman);
			gctx.ShowTextAtPoint((wPage/2), -(hPage - 10) ,"Page 1");
			UIGraphics.EndPDFContent ();

			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string pdfFilePath = System.IO.Path.Combine (documentsDirectory, "tmp.pdf"); 
			NSError err = null;
			if (data.Save(pdfFilePath, false, out err)) {
				Console.WriteLine("Saved as " + pdfFilePath);
			} else {
				Console.WriteLine("NOT saved as " + pdfFilePath + " because" + err.LocalizedDescription);
			}
			return data;
		}

		public static void deletePDFTmp(){
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string pdfFilePath = System.IO.Path.Combine (documentsDirectory, "tmp.pdf");
			System.IO.File.Delete (pdfFilePath);
		}

		public static void autoGenSize(UIImage image){
			if ((float)image.Size.Width > (wPage - 30) || (float)image.Size.Height > (hPage - 30)) {
				if (image.Size.Width >= image.Size.Height) {
					wImage = wPage - 30;
					hImage = (float)image.Size.Height * (wPage) / (float)image.Size.Width  - 30;
				} else {
					hImage = hPage - 30;
					wImage = (float)image.Size.Width * (hPage) / (float)image.Size.Height - 30;
				}
			} else {
				wImage = (float)image.Size.Width;
				hImage = (float)image.Size.Height;
			}
		}

	}
}

