﻿using System;
using UIKit;
using CoreGraphics;
using System.Drawing;
using System.Collections.Generic;

namespace XaScanner_IOS
{
	public static class ImageUtils
	{

		public static UIImage ScaleAndRotateImage(UIImage imageIn, UIImageOrientation orIn) {
			int kMaxResolution = 2048;

			CGImage imgRef = imageIn.CGImage;
			float width = imgRef.Width;
			float height = imgRef.Height;
			CGAffineTransform transform = CGAffineTransform.MakeIdentity ();
			RectangleF bounds = new RectangleF( 0, 0, width, height );


			float scaleRatio = bounds.Width / width;
			SizeF imageSize = new SizeF( width, height);
			UIImageOrientation orient = orIn;
			float boundHeight;

			switch(orient)
			{
			case UIImageOrientation.Up:                                        //EXIF = 1
				transform = CGAffineTransform.MakeIdentity();
				break;

			case UIImageOrientation.UpMirrored:                                //EXIF = 2
				transform = CGAffineTransform.MakeTranslation (imageSize.Width, 0f);
				transform = CGAffineTransform.MakeScale(-1.0f, 1.0f);
				break;

			case UIImageOrientation.Down:                                      //EXIF = 3
				transform = CGAffineTransform.MakeTranslation (imageSize.Width, imageSize.Height);
				transform = CGAffineTransform.Rotate(transform, (float)Math.PI);
				break;

			case UIImageOrientation.DownMirrored:                              //EXIF = 4
				transform = CGAffineTransform.MakeTranslation (0f, imageSize.Height);
				transform = CGAffineTransform.MakeScale(1.0f, -1.0f);
				break;

			case UIImageOrientation.LeftMirrored:                              //EXIF = 5
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation (imageSize.Height, imageSize.Width);
				transform = CGAffineTransform.MakeScale(-1.0f, 1.0f);
				transform = CGAffineTransform.Rotate(transform, 3.0f * (float)Math.PI/ 2.0f);
				break;

			case UIImageOrientation.Left:                                      //EXIF = 6
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation (0.0f, imageSize.Width);
				transform = CGAffineTransform.Rotate(transform, 3.0f * (float)Math.PI / 2.0f);
				break;

			case UIImageOrientation.RightMirrored:                             //EXIF = 7
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeScale(-1.0f, 1.0f);
				transform = CGAffineTransform.Rotate(transform, (float)Math.PI / 2.0f);
				break;

			case UIImageOrientation.Right:                                     //EXIF = 8
				boundHeight = bounds.Height;
				bounds.Height = bounds.Width;
				bounds.Width = boundHeight;
				transform = CGAffineTransform.MakeTranslation(imageSize.Height, 0.0f);
				transform = CGAffineTransform.Rotate(transform, (float)Math.PI  / 2.0f);
				break;

			default:
				throw new Exception("Invalid image orientation");
				break;
			}

			UIGraphics.BeginImageContext(bounds.Size);

			CGContext context = UIGraphics.GetCurrentContext ();

			if ( orient == UIImageOrientation.Right || orient == UIImageOrientation.Left )
			{
				context.ScaleCTM(-scaleRatio, scaleRatio);
				context.TranslateCTM(-height, 0);
			}
			else
			{
				context.ScaleCTM(scaleRatio, -scaleRatio);
				context.TranslateCTM(0, -height);
			}

			context.ConcatCTM(transform);
			context.DrawImage (new RectangleF (0, 0, width, height), imgRef);

			UIImage imageCopy = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();

			return imageCopy;
		}



		// resize the image (without trying to maintain aspect ratio)
		public static UIImage ResizeImage(UIImage sourceImage, float width, float height)
		{
			UIGraphics.BeginImageContext(new SizeF(width, height));
			sourceImage.Draw(new RectangleF(0, 0, width, height));
			var resultImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return resultImage;
		}

		// crop the image, without resizing
		public static UIImage CropImage(UIImage sourceImage, int crop_x, int crop_y, int width, int height)
		{
			var imgSize = sourceImage.Size;
			UIGraphics.BeginImageContext(new SizeF(width, height));
			var context = UIGraphics.GetCurrentContext();
			var clippedRect = new RectangleF(0, 0, width, height);
			context.ClipToRect(clippedRect);
			var drawRect = new RectangleF(-crop_x, -crop_y, (float)imgSize.Width, (float)imgSize.Height);
			sourceImage.Draw(drawRect);
			var modifiedImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();
			return modifiedImage;
		}

		public static UIImage getImageByName(string fileName){
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string imagePath = System.IO.Path.Combine (documentsDirectory, fileName);
			return UIImage.FromFile (imagePath);
		}

		public static void deleteImageInDirectoryByName(string fileName)
		{
			var documentsDirectory = Environment.GetFolderPath
				(Environment.SpecialFolder.Personal);
			string imagePath = System.IO.Path.Combine (documentsDirectory, fileName);
			System.IO.File.Delete (imagePath);
		}

		public static CGPoint PixelPoint2ViewPoint(CGPoint _imagePoint, UIImageView view)
		{
			CGPoint viewPoint = _imagePoint;
			CGSize imageSize = view.Image.Size;
			CGSize viewSize  = view.Bounds.Size;
			nfloat ratioX = viewSize.Width / imageSize.Width;
			nfloat ratioY = viewSize.Height / imageSize.Height;

			switch (view.ContentMode) 
			{
			case UIViewContentMode.ScaleToFill:
			case UIViewContentMode.Redraw:
				viewPoint.X *= ratioX;
				viewPoint.Y *= ratioY;
				break;

			case UIViewContentMode.ScaleAspectFill:
			case UIViewContentMode.ScaleAspectFit: 
				{
					nfloat scale = 1;

					if (view.ContentMode == UIViewContentMode.ScaleAspectFit) {
						scale = Math.Min((float)ratioX, (float)ratioY);
					}
					else {
						scale = Math.Max((float)ratioX, (float)ratioY);
					}

					viewPoint.X *= scale;
					viewPoint.Y *= scale;
					// Remove the x or y margin added in FitMode
					viewPoint.X += (viewSize.Width  - imageSize.Width  * scale) / 2.0f;
					viewPoint.Y += (viewSize.Height - imageSize.Height * scale) / 2.0f;


					break;
				}
			}
			viewPoint.X += view.Frame.Location.X;
			viewPoint.Y += view.Frame.Location.Y;
			return viewPoint;
		}

		public static CGPoint ViewPoint2PixelPoint(CGPoint viewPoint, UIImageView view)
		{
			CGPoint imagePoint = viewPoint;
			CGSize imageSize = view.Image.Size;
			CGSize viewSize  = view.Bounds.Size;
			nfloat ratioX = viewSize.Width / imageSize.Width;
			nfloat ratioY = viewSize.Height / imageSize.Height;

			switch (view.ContentMode) 
			{
			case UIViewContentMode.ScaleToFill:
			case UIViewContentMode.Redraw:
				imagePoint.X /= ratioX;
				imagePoint.Y /= ratioY;
				break;

			case UIViewContentMode.ScaleAspectFill:
			case UIViewContentMode.ScaleAspectFit: 
				{
					nfloat scale = 1;

					if (view.ContentMode == UIViewContentMode.ScaleAspectFit) {
						scale = Math.Min((float)ratioX, (float)ratioY);
					}
					else {
						scale = Math.Max((float)ratioX, (float)ratioY);
					}

					// Remove the x or y margin added in FitMode
					imagePoint.X -= (viewSize.Width  - imageSize.Width  * scale) / 2.0f;
					imagePoint.Y -= (viewSize.Height - imageSize.Height * scale) / 2.0f;

					imagePoint.X /= scale;
					imagePoint.Y /= scale;
					break;
				}
			}
			return imagePoint;
		}

		public static bool isOutOfImage(CGPoint viewPoint, UIImageView view)
		{
			if (viewPoint.X < view.Frame.Location.X || viewPoint.Y < view.Frame.Location.Y) {
				return true;
			}
			CGSize imageSize = view.Image.Size;
			CGSize viewSize  = view.Bounds.Size;
			nfloat ratioX = viewSize.Width / imageSize.Width;
			nfloat ratioY = viewSize.Height / imageSize.Height;


			switch (view.ContentMode) 
			{
			case UIViewContentMode.ScaleToFill:
			case UIViewContentMode.Redraw:
				{
					if (viewPoint.X < view.Frame.Location.X || viewPoint.Y < view.Frame.Location.Y
						|| viewPoint.X > (view.Frame.Location.X + view.Bounds.Width)
					   || viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height)) {
						return true;
					}
					break;
				}

			case UIViewContentMode.ScaleAspectFill:
			case UIViewContentMode.ScaleAspectFit: 
				{
					nfloat scale = 1;

					if (view.ContentMode == UIViewContentMode.ScaleAspectFit) {
						scale = Math.Min((float)ratioX, (float)ratioY);
					}
					else {
						scale = Math.Max((float)ratioX, (float)ratioY);
					}

					var paddingW = (viewSize.Width  - imageSize.Width  * scale) / 2.0f;
					var paddingH = (viewSize.Height  - imageSize.Height  * scale) / 2.0f;
					if (viewPoint.X < (view.Frame.Location.X + paddingW) || viewPoint.X > (view.Frame.Location.X + view.Bounds.Width - paddingW)
					   || viewPoint.Y < (view.Frame.Location.Y + paddingH) || viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height - paddingH)) 
					{
						return true;
					}
					
					break;
				}
			}
			return false;
			
		}
		/// <summary>
		/// Validates the out of image.
		/// </summary>
		/// <returns>The out of image.</returns>
		/// <param name="viewPoint">View point.</param>
		/// <param name="view">View.</param>
		public static CGPoint validateOutOfImage(CGPoint viewPoint, UIImageView view)
		{
			CGSize imageSize = view.Image.Size;
			CGSize viewSize  = view.Bounds.Size;
			nfloat ratioX = viewSize.Width / imageSize.Width;
			nfloat ratioY = viewSize.Height / imageSize.Height;

			switch (view.ContentMode) 
			{
			case UIViewContentMode.ScaleToFill:
			case UIViewContentMode.Redraw:
				{
					if (viewPoint.X < view.Frame.Location.X) {
						viewPoint.X = view.Frame.Location.X;
					} 
					if (viewPoint.Y < view.Frame.Location.Y) {
						viewPoint.Y = view.Frame.Location.Y;
					}
					if (viewPoint.X > (view.Frame.Location.X + view.Bounds.Width)) {
						viewPoint.X = (view.Frame.Location.X + view.Bounds.Width);
					}
					if(viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height)) {
						viewPoint.Y = (view.Frame.Location.Y + view.Bounds.Height);
					}
					break;
				}

			case UIViewContentMode.ScaleAspectFill:
			case UIViewContentMode.ScaleAspectFit: 
				{
					nfloat scale = 1;

					if (view.ContentMode == UIViewContentMode.ScaleAspectFit) {
						scale = Math.Min((float)ratioX, (float)ratioY);
					}
					else {
						scale = Math.Max((float)ratioX, (float)ratioY);
					}

					var paddingW = (viewSize.Width  - imageSize.Width  * scale) / 2.0f;
					var paddingH = (viewSize.Height  - imageSize.Height  * scale) / 2.0f;

					if (viewPoint.X < (view.Frame.Location.X + paddingW)){
						viewPoint.X = (view.Frame.Location.X + paddingW);
					} 
					if (viewPoint.X > (view.Frame.Location.X + view.Bounds.Width - paddingW)) {
						viewPoint.X = (view.Frame.Location.X + view.Bounds.Width - paddingW);
					}
					if (viewPoint.Y < (view.Frame.Location.Y + paddingH)) {
						viewPoint.Y = (view.Frame.Location.Y + paddingH);
					}
					if(viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height - paddingH)) 
					{
						viewPoint.Y = (view.Frame.Location.Y + view.Bounds.Height - paddingH);
					}

					break;
				}
			}
			return viewPoint;

		}

		public static bool checkOutOfImage(CGPoint viewPoint, UIImageView view)
		{
			CGSize imageSize = view.Image.Size;
			CGSize viewSize  = view.Bounds.Size;
			nfloat ratioX = viewSize.Width / imageSize.Width;
			nfloat ratioY = viewSize.Height / imageSize.Height;

			switch (view.ContentMode) 
			{
			case UIViewContentMode.ScaleToFill:
			case UIViewContentMode.Redraw:
				{
					if (viewPoint.X < view.Frame.Location.X) {
						return false;
					} 
					if (viewPoint.Y < view.Frame.Location.Y) {
						return false;
					}
					if (viewPoint.X > (view.Frame.Location.X + view.Bounds.Width)) {
						return false;
					}
					if(viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height)) {
						return false;
					}
					break;
				}

			case UIViewContentMode.ScaleAspectFill:
			case UIViewContentMode.ScaleAspectFit: 
				{
					nfloat scale = 1;

					if (view.ContentMode == UIViewContentMode.ScaleAspectFit) {
						scale = Math.Min((float)ratioX, (float)ratioY);
					}
					else {
						scale = Math.Max((float)ratioX, (float)ratioY);
					}

					var paddingW = (viewSize.Width  - imageSize.Width  * scale) / 2.0f;
					var paddingH = (viewSize.Height  - imageSize.Height  * scale) / 2.0f;

					if (viewPoint.X < (view.Frame.Location.X + paddingW)){
						return false;
					} 
					if (viewPoint.X > (view.Frame.Location.X + view.Bounds.Width - paddingW)) {
						return false;
					}
					if (viewPoint.Y < (view.Frame.Location.Y + paddingH)) {
						return false;
					}
					if(viewPoint.Y > (view.Frame.Location.Y + view.Bounds.Height - paddingH)) 
					{
						return false;
					}
					break;
				}
			}
			return true;

		}


		public static List<CGPoint> sortByXY(List<CGPoint> listCGPoints, bool isByX)
		{
			List<CGPoint> listSorted = listCGPoints;

			CGPoint temp = new CGPoint ();
			for (int i = 0; i < listSorted.Count; i++) {
				for (int j = (listSorted.Count - 1); j > i; j--) {
					if (isByX) {
						if (listSorted [j].X < listSorted [j - 1].X) {
							temp = listSorted [j];
							listSorted [j] = listSorted [j - 1];
							listSorted [j - 1] = temp;
						}
					} else {
						if (listSorted [j].Y < listSorted [j - 1].Y) {
							temp = listSorted [j];
							listSorted [j] = listSorted [j - 1];
							listSorted [j - 1] = temp;
						}
					}

				}
			}
			return listSorted;
		}


	}
}