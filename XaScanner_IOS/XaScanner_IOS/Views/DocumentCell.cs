﻿
using System;

using Foundation;
using UIKit;

namespace XaScanner_IOS
{

	public partial class DocumentCell : UICollectionViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("DocumentCell", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("DocumentCell");

		public DocumentCell (IntPtr handle) : base (handle)
		{
			imgSelected.Image = UIImage.FromBundle ("selected.png");
		}

		public static DocumentCell Create ()
		{
			return (DocumentCell)Nib.Instantiate (null, null) [0];
		}


		public UIImage Image {
			set {
				imgSelected.Image = value;
			}
		}

		public bool ShowSelected{
			set {
				imgSelected.Hidden = value; 
			}
		}

		public UIImage ImageBackground {
			set {
				imgBgr.Image = value;
			}
		}
	}
}

