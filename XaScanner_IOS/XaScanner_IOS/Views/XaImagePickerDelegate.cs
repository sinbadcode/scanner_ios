﻿using System;
using UIKit;
using Foundation;

namespace XaScanner_IOS
{
	public class XaImagePickerDelegate: UIImagePickerControllerDelegate 
	{
		/// <summary>
		/// The vc.
		/// </summary>
		UIViewController _vc; 

		/// <summary>
		/// Initializes a new instance of the <see cref="XaScanner.XaImagePickerDelegate"/> class.
		/// </summary>
		public XaImagePickerDelegate () 
		{ 
		} 

		/// <summary>
		/// Initializes a new instance of the <see cref="XaScanner.XaImagePickerDelegate"/> class.
		/// </summary>
		/// <param name="vc">Vc.</param>
		public XaImagePickerDelegate(UIViewController vc) 
		{ 
			_vc = vc;	
		} 

		/// <Docs>To be added.</Docs>
		/// <remarks>To be added.</remarks>
		/// <summary>
		/// Determines whether this instance canceled  picker.
		/// </summary>
		/// <returns><c>true</c> if this instance canceled picker; otherwise, <c>false</c>.</returns>
		/// <param name="picker">Picker.</param>
		public override void Canceled (UIImagePickerController picker) 
		{ 
			_vc.DismissViewController (true, null);
		} 

		/// <Docs>To be added.</Docs>
		/// <summary>
		/// Indicates that the user has picked a picture or movie.
		/// </summary>
		/// <param name="picker">Picker.</param>
		/// <param name="info">Info.</param>
		public override void FinishedPickingMedia (UIImagePickerController picker, NSDictionary info) 
		{ 
			UIImage image = info.ObjectForKey (UIImagePickerController.OriginalImage) as UIImage;
			var _w = 1200;
			var _h = (image.Size.Height * (_w/image.Size.Width));
			//XaImageContext.OriginalImage = XaImageContext.Clone(image,UIImageOrientation.Up);
			image = ImageUtils.ResizeImage (image, (float)_w, (int) _h);
			XaImageContext.OriginalImage  = image;
			image = null;
			_vc.DismissViewController (false, null);
			if(_vc.GetType() == typeof(XaMainScannerVC)){
				_vc.PerformSegue ("Push2ResizingPhoto", _vc);	
			}else if(_vc.GetType() == typeof(XaCropImageCV)){
				((XaCropImageCV)_vc).autoDetectInProgress ();	
			}
		} 
	}
}

