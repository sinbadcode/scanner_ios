﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;

namespace XaScanner_IOS
{
	public class MyCell: UICollectionViewCell
	{
		UIImageView imageView;

		[Export ("initWithFrame:")]
		public MyCell (CGRect frame) : base (frame)
		{
			BackgroundView = new UIImageView (UIImage.FromBundle ("error.png"));
			ContentView.Layer.BorderWidth = 2.0f;
			imageView = new UIImageView (UIImage.FromBundle ("selected.png")); 
			imageView.Center = ContentView.Center;
			imageView.Hidden = true;
			ContentView.AddSubview (imageView);
		}
		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			imageView.Frame = new CGRect (ContentView.Bounds.Width - 60, 10, 50, 50);
		}
		public UIImage Image {
			set {
				imageView.Image = value;
			}
		}

		public bool ShowSelected{
			set {
				imageView.Hidden = value; 
			}
		}

		public UIImage ImageBackground {
			set {
				BackgroundView = new UIImageView (value);
			}
		}
	}
}

