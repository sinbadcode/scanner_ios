			using System;
using System.Drawing;

using Foundation;
using UIKit;
using CoreGraphics;
using CoreAnimation;
using MonoTouch.GPUImage;
using MonoTouch.GPUImage.Filters;
using AGGeometryKit;
using SQLite.Net;
using SQLite.Net.Platform.XamarinIOS;
using System.Threading.Tasks;
using SCImage;
using System.Collections.Generic;

namespace XaScanner_IOS
{
	partial class XaCropImageCV : UIViewController
	{


		LoadingOverlay loadingOverlay;
		NSArray listPoints = new NSArray ();
		/// <summary>
		/// Gets or sets the shape layer.
		/// </summary>
		/// <value>The shape layer.</value>
		CAShapeLayer shapeLayer { get; set;}

		/// <summary>
		/// Gets the point view0.
		/// </summary>
		/// <value>The point view0.</value>
		UIView pointView0
		{
			get 
			{ 
				return this.View.ViewWithTag (101);
			}
		}

		/// <summary>
		/// Gets the point view0.
		/// </summary>
		/// <value>The point view0.</value>
		UIView pointView1
		{
			get 
			{ 
				return this.View.ViewWithTag (102);
			}
		}

		/// <summary>
		/// Gets the point view0.
		/// </summary>
		/// <value>The point view0.</value>
		UIView pointView2
		{
			get 
			{ 
				return this.View.ViewWithTag (103);
			}
		}

		/// <summary>
		/// Gets the point view0.
		/// </summary>
		/// <value>The point view0.</value>
		UIView pointView3
		{
			get 
			{ 
				return this.View.ViewWithTag (104);
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="XaScanner.XaResizingPhotoVC"/> class.
		/// </summary>
		/// <param name="handle">Handle.</param>
		public XaCropImageCV (IntPtr handle) : base (handle)
		{
		}

		/// <summary>
		/// Gets the image view.
		/// </summary>
		/// <value>The image view.</value>
		UIImageView imageView
		{
			get
			{ 
				return this.View.ViewWithTag (1000) as UIImageView;
			}
		}

		/// <summary>
		/// Adds the gestures.
		/// </summary>
		void addGestures()
		{
			pointView0.UserInteractionEnabled = true;
			pointView1.UserInteractionEnabled = true;
			pointView2.UserInteractionEnabled = true;
			pointView3.UserInteractionEnabled = true;

			UIPanGestureRecognizer pan1 = new UIPanGestureRecognizer (onPan);
			pointView0.AddGestureRecognizer (pan1);
			UIPanGestureRecognizer pan2 = new UIPanGestureRecognizer (onPan);
			pointView1.AddGestureRecognizer (pan2);
			UIPanGestureRecognizer pan3 = new UIPanGestureRecognizer (onPan);
			pointView2.AddGestureRecognizer (pan3);
			UIPanGestureRecognizer pan4 = new UIPanGestureRecognizer (onPan);
			pointView3.AddGestureRecognizer (pan4);
		}

		/// <summary>
		/// Adds the shape layer.
		/// </summary>
		void addShapeLayer()
		{
			this.shapeLayer = new CAShapeLayer ();
			shapeLayer.StrokeColor = UIColor.FromRGBA (0, 0, 0.8f, 0.5f).CGColor;
			shapeLayer.LineWidth = 4;
			shapeLayer.FillColor = UIColor.Clear.CGColor;
			imageView.Layer.AddSublayer (shapeLayer);
		}

		public void autoDetectInProgress(){
			pointViewsLayout ();
			drawQuad ();
			this.imageView.Image = XaImageContext.OriginalImage;
			this.addGestures ();
			// Determine the correct size to start the overlay (depending on device orientation)
			var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
			if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
				bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
			}
			// show the loading overlay on the UI thread using the correct orientation sizing
			this.loadingOverlay = new LoadingOverlay (bounds);
			this.View.Add ( this.loadingOverlay );

			//			// spin up a new thread to do some long running work using StartNew
			Task.Factory.StartNew (
				// tasks allow you to use the lambda syntax to pass work
				() => {
					//Console.WriteLine ( "Run in background to detect doc" );
					UIImage drawOutImage = new UIImage();
					var tryNumber = 0;
					listPoints = SCImage.SCImage.detectEdges(XaImageContext.OriginalImage, out drawOutImage);
					while((tryNumber < 5) && (!this.validateDetectedPoint(listPoints))){
						if(tryNumber % 2 == 0)
							listPoints = SCImage.SCImage.detectQuadOfImage(XaImageContext.OriginalImage, out drawOutImage);
						else 
							listPoints = SCImage.SCImage.DetectQuadFormage(XaImageContext.OriginalImage, out drawOutImage);
						tryNumber++;
					}
					XaImageContext.CroppedImage = XaImageContext.Clone(drawOutImage);
					drawOutImage = null;
				}
			).ContinueWith ( 
				t => {
					loadingOverlay.Hide ();
					//Console.WriteLine ( "Finished, Update UI"  );
					setDetectedCropToPan ();
					//this.imageView.Image = XaImageContext.CroppedImage;
				}, TaskScheduler.FromCurrentSynchronizationContext()
			);
		}

		void setDetectedCropToPan(){
			List<CGPoint> listCGPoints = new List<CGPoint> ();
			foreach (var point in NSArray.FromArray <CvPoint> (listPoints)) {
				CGPoint tCGP = new CGPoint (point.X, point.Y);
				CGPoint tCGP2 = ImageUtils.PixelPoint2ViewPoint (tCGP, this.imageView);
				listCGPoints.Add (tCGP2);
			}
			sort4Pans(listCGPoints);
			drawQuad ();
		}

		/// <summary>
		/// Points the views layout.
		/// </summary>
		void pointViewsLayout()
		{
			this.pointView0.Center = imageView.Frame.Location;
			this.pointView1.Center = new CGPoint (imageView.Frame.Location.X + imageView.Frame.Size.Width, imageView.Frame.Location.Y);
			this.pointView2.Center = new CGPoint (imageView.Frame.Location.X + imageView.Frame.Size.Width,  imageView.Frame.Location.Y + imageView.Frame.Size.Height);
			this.pointView3.Center = new CGPoint (imageView.Frame.Location.X,  imageView.Frame.Location.Y + imageView.Frame.Size.Height);
		}

		/// <Docs>Called when the system is running low on memory.</Docs>
		/// <summary>
		/// Dids the receive memory warning.
		/// </summary>
		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();

			// Release any cached data, images, etc that aren't in use.
		}

		/// <summary>
		/// Views the did load.
		/// </summary>
		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			btnRetake.TouchUpInside += (object sender, EventArgs e) =>{
				UIImagePickerController picker = new UIImagePickerController ();
				picker.SourceType = UIImagePickerControllerSourceType.Camera;
				picker.Delegate = new XaImagePickerDelegate(this);
				picker.CameraDevice = UIImagePickerControllerCameraDevice.Rear;
				this.PresentViewController(picker,true,null);
			};

			btnRotateLeft.TouchUpInside += delegate {
				this.imageView.Image = UIImage.FromImage(this.imageView.Image.CGImage, this.imageView.Image.CurrentScale, UIImageOrientation.Left);
				var resultImage = ImageUtils.ScaleAndRotateImage(this.imageView.Image, UIImageOrientation.Left);
				this.imageView.Image = resultImage;
				XaImageContext.OriginalImage = resultImage;
				resultImage = null;
				rotateLeftPan();
				UIGraphics.EndImageContext();
			};

			btnRotateRight.TouchUpInside += delegate {
				this.imageView.Image = UIImage.FromImage(this.imageView.Image.CGImage, this.imageView.Image.CurrentScale, UIImageOrientation.Right);
				var resultImage = ImageUtils.ScaleAndRotateImage(this.imageView.Image, UIImageOrientation.Right);
				this.imageView.Image = resultImage;
				XaImageContext.OriginalImage = resultImage;
				resultImage = null;
				rotateRightPan();
				UIGraphics.EndImageContext();
			};
			this.imageView.ContentMode = UIViewContentMode.ScaleAspectFit;
			this.imageView.Image = XaImageContext.OriginalImage;
			pointViewsLayout ();
			this.addShapeLayer ();
			this.autoDetectInProgress ();


		}

		/// <summary>
		/// Views the will appear.
		/// </summary>
		/// <param name="animated">If set to <c>true</c> animated.</param>
		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear (animated);
		}


		void rotateRightPan(){
			//(imageView.Frame.Size.Width - this.pointView0.Center.Y - imageView.Frame.Location.Y + imageView.Frame.Location.X);
			CGPoint p1 = new CGPoint(imageView.Frame.Size.Width - this.pointView0.Center.Y + imageView.Frame.Location.Y + imageView.Frame.Location.X, this.pointView0.Center.X + imageView.Frame.Location.Y - imageView.Frame.Location.X);
			CGPoint p2 = new CGPoint(imageView.Frame.Size.Width - this.pointView1.Center.Y + imageView.Frame.Location.Y + imageView.Frame.Location.X, this.pointView1.Center.X + imageView.Frame.Location.Y - imageView.Frame.Location.X);
			CGPoint p3 = new CGPoint(imageView.Frame.Size.Width - this.pointView2.Center.Y + imageView.Frame.Location.Y + imageView.Frame.Location.X, this.pointView2.Center.X + imageView.Frame.Location.Y - imageView.Frame.Location.X);
			CGPoint p0 = new CGPoint(imageView.Frame.Size.Width - this.pointView3.Center.Y + imageView.Frame.Location.Y + imageView.Frame.Location.X, this.pointView3.Center.X + imageView.Frame.Location.Y - imageView.Frame.Location.X);
			this.pointView0.Center = p0;
			this.pointView3.Center = p3;
			this.pointView2.Center = p2;
			this.pointView1.Center = p1;
			//p1 = p2 = p3 = p0 = null;
			this.drawQuad ();
		}
		void rotateLeftPan(){
			CGPoint p3 = new CGPoint(this.pointView0.Center.Y + imageView.Frame.Location.X - imageView.Frame.Location.Y, imageView.Frame.Location.Y + imageView.Frame.Size.Height - this.pointView0.Center.X + imageView.Frame.Location.X);
			CGPoint p0 = new CGPoint(this.pointView1.Center.Y + imageView.Frame.Location.X - imageView.Frame.Location.Y, imageView.Frame.Location.Y + imageView.Frame.Size.Height - this.pointView1.Center.X + imageView.Frame.Location.X);
			CGPoint p1 = new CGPoint(this.pointView2.Center.Y + imageView.Frame.Location.X - imageView.Frame.Location.Y, imageView.Frame.Location.Y + imageView.Frame.Size.Height - this.pointView2.Center.X + imageView.Frame.Location.X);
			CGPoint p2 = new CGPoint(this.pointView3.Center.Y + imageView.Frame.Location.X - imageView.Frame.Location.Y, imageView.Frame.Location.Y + imageView.Frame.Size.Height - this.pointView3.Center.X + imageView.Frame.Location.X);
			this.pointView0.Center = p0;
			this.pointView3.Center = p3;
			this.pointView2.Center = p2;
			this.pointView1.Center = p1;
			this.drawQuad ();
		}
		/// <summary>
		/// Raises the pan event.
		/// </summary>
		/// <param name="pan">Pan.</param>
		void onPan(UIPanGestureRecognizer pan)
		{
			UIImageView pv = pan.View as UIImageView;
			CGPoint pt = pan.TranslationInView (View);
			pv.Center = new CGPoint (pv.Center.X + pt.X, pv.Center.Y + pt.Y);
			pan.SetTranslation (CGPoint.Empty, View);
			drawQuad ();
		}

		/// <summary>
		/// Draws the quad.
		/// </summary>
		void drawQuad()
		{
			pointView0.Center = ImageUtils.validateOutOfImage (pointView0.Center, this.imageView);
			pointView1.Center = ImageUtils.validateOutOfImage (pointView1.Center, this.imageView);
			pointView2.Center = ImageUtils.validateOutOfImage (pointView2.Center, this.imageView);
			pointView3.Center = ImageUtils.validateOutOfImage (pointView3.Center, this.imageView);

			CGPoint p0 = imageView.ConvertPointFromView (pointView0.Center, View);
			CGPoint p1 = imageView.ConvertPointFromView (pointView1.Center, View);
			CGPoint p2 = imageView.ConvertPointFromView (pointView2.Center, View);
			CGPoint p3 = imageView.ConvertPointFromView (pointView3.Center, View);

			UIBezierPath bpath = new UIBezierPath ();
			bpath.MoveTo (p0);
			bpath.AddLineTo (p1);
			bpath.AddLineTo (p2);
			bpath.AddLineTo (p3);
			bpath.ClosePath ();

			shapeLayer.Path = bpath.CGPath;
		}


		CvPoint convertCGPoint2CVPoint(CGPoint cgP){
			CGPoint tCGP = new CGPoint (cgP.X - imageView.Frame.Location.X, cgP.Y - imageView.Frame.Location.Y);

			CGPoint tCGP2 = ImageUtils.ViewPoint2PixelPoint (tCGP, this.imageView); 
			CvPoint cvP = new CvPoint();
			cvP.X = (int)tCGP2.X;
			cvP.Y = (int)tCGP2.Y;
			//tIMG = null;
			return cvP;
		}

		void sort4Pans(List<CGPoint> listCGPoints){
			List<CGPoint> listSortedByY = ImageUtils.sortByXY (listCGPoints, false);

			if (listSortedByY [0].X < listSortedByY [1].X) {
				this.pointView0.Center = new CGPoint (listSortedByY [0].X, listSortedByY [0].Y);
				this.pointView1.Center = new CGPoint (listSortedByY[1].X, listSortedByY[1].Y);
			} else {
				this.pointView0.Center = new CGPoint (listSortedByY [1].X, listSortedByY [1].Y);
				this.pointView1.Center = new CGPoint (listSortedByY[0].X, listSortedByY[0].Y);
			}

			if (listSortedByY [2].X > listSortedByY [3].X) {
				this.pointView2.Center = new CGPoint (listSortedByY [2].X, listSortedByY [2].Y);
				this.pointView3.Center = new CGPoint (listSortedByY[3].X, listSortedByY[3].Y);
			} else {
				this.pointView2.Center = new CGPoint (listSortedByY [3].X,listSortedByY [3].Y);
				this.pointView3.Center = new CGPoint (listSortedByY[2].X, listSortedByY[2].Y);
			}
		}

		bool validateDetectedPoint(NSArray listPoints){
			if (listPoints.Count < 4) {
				return false;
			}
			List<CGPoint> listCGPoints = new List<CGPoint> ();
			foreach (var point in NSArray.FromArray <CvPoint> (listPoints)) {
				CGPoint tCGP = new CGPoint (point.X, point.Y);
				listCGPoints.Add (tCGP);
			}
			if ((!ImageUtils.checkOutOfImage(listCGPoints [0], this.imageView))
				|| (!ImageUtils.checkOutOfImage(listCGPoints [1], this.imageView))
				|| (!ImageUtils.checkOutOfImage(listCGPoints [2], this.imageView))
				|| (!ImageUtils.checkOutOfImage(listCGPoints [3], this.imageView))) {
				return false;
			}
			if ((Commom.distance (listCGPoints [0], listCGPoints [1]) < 100)
				|| (Commom.distance (listCGPoints [0], listCGPoints [2]) < 100)
				|| (Commom.distance (listCGPoints [0], listCGPoints [3]) < 100)
				|| (Commom.distance (listCGPoints [1], listCGPoints [2]) < 100)
				|| (Commom.distance (listCGPoints [1], listCGPoints [3]) < 100)
				|| (Commom.distance (listCGPoints [2], listCGPoints [3]) < 100)) {
				return false;
			} else {
				return true;
			}
			
		}


		/// <summary>
		/// Raises the done event.
		/// </summary>
		/// <param name="sender">Sender.</param>
		partial void OnDone (UIButton sender)
		{
			List<CGPoint> listCGPoints = new List<CGPoint>();
			listCGPoints.Add(pointView0.Center);
			listCGPoints.Add(pointView1.Center);
			listCGPoints.Add(pointView2.Center);
			listCGPoints.Add(pointView3.Center);
			sort4Pans(listCGPoints);

			CvPoint cvP0 = new CvPoint();
			cvP0 = convertCGPoint2CVPoint(pointView0.Center);

			CvPoint cvP1 = new CvPoint();
			cvP1 = convertCGPoint2CVPoint(pointView1.Center);

			CvPoint cvP2 = new CvPoint();
			cvP2 = convertCGPoint2CVPoint(pointView2.Center);

			CvPoint cvP3 = new CvPoint();
			cvP3 = convertCGPoint2CVPoint(pointView3.Center);

			NSArray fourPoints = NSArray.FromObjects(cvP0, cvP1, cvP2, cvP3);

			UIImage tIMG = SCImage.SCImage.FixRotationForImage(XaImageContext.OriginalImage);
			UIImage croppedImage = SCImage.SCImage.WarpImage(tIMG,fourPoints);

			XaImageContext.CroppedImage = croppedImage;
			tIMG = null;
			this.PerformSegue ("Push2FiltersPhoto", this);

		}
	}
}
