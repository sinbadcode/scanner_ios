// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace XaScanner_IOS
{
	[Register ("XaCropImageCV")]
	partial class XaCropImageCV
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnRetake { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnRotateLeft { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnRotateRight { get; set; }

		[Action ("OnDone:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnDone (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btnRetake != null) {
				btnRetake.Dispose ();
				btnRetake = null;
			}
			if (btnRotateLeft != null) {
				btnRotateLeft.Dispose ();
				btnRotateLeft = null;
			}
			if (btnRotateRight != null) {
				btnRotateRight.Dispose ();
				btnRotateRight = null;
			}
		}
	}
}
