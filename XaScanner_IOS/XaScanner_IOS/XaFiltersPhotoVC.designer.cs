// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace XaScanner_IOS
{
	[Register ("XaFiltersPhotoVC")]
	partial class XaFiltersPhotoVC
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnDone { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnDoneAndView { get; set; }

		[Action ("OnAutoFilter:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnAutoFilter (UIButton sender);

		[Action ("OnBWFilter:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnBWFilter (UIButton sender);

		[Action ("OnGrayscaleFilter:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnGrayscaleFilter (UIButton sender);

		[Action ("OnLightenFilter:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void OnLightenFilter (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btnDone != null) {
				btnDone.Dispose ();
				btnDone = null;
			}
			if (btnDoneAndView != null) {
				btnDoneAndView.Dispose ();
				btnDoneAndView = null;
			}
		}
	}
}
